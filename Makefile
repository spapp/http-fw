
default: test

test:
	cd test;phpunit

doc:
	/usr/bin/phpdoc -c phpdoc.xml
	rm -rf docs/phpdoc-cache*

.PHONY: default test doc