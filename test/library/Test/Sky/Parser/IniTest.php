<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.05.20.
 */
namespace Test\Sky;

use PHPUnit_Framework_TestCase as TestCase;
use Sky\Parser\Ini as IniParser;

/**
 * Class IniTest
 */
class IniTest extends TestCase {
    protected static $ini = <<<CONTENT
[twoInherits:development]
database.db1.dbName = twoInherits-db
[production]
testKey=15
database.db1.host = localhost
database.db1.dbName = dbName-prod
database.db1.user = username-prod

[staging : testing]
this.is.the.very.very.very.very.deep.deep.end=1
this.is.the.very.very.very.very.deep.end=2
this.is.the.very.very.very.very.end.0=0
this.is.the.very.very.very.very.end.1=1
this.is.the.very.very.very.very.end.2=2

[testing: production]
database.db1.host = test-server

[development:production]
database.db1.host = 127.0.0.1

[error:not_exists]
database.db1.host = error-server
[error_2:]
database.db1.host = error_2-server
CONTENT;

    public function testParse() {
        $ini = new IniParser(self::$ini);

        $p = $ini->toArray();

        $this->assertTrue(is_array($p['twoInherits']));
        $this->assertTrue(is_array($p['production']));
        $this->assertTrue(is_array($p['staging']));
        $this->assertTrue(is_array($p['testing']));
        $this->assertTrue(is_array($p['development']));
        $this->assertTrue(is_array($p['error']));
        $this->assertTrue(is_array($p['error_2']));

        $this->assertTrue(
                isset($p['staging']['this']['is']['the']['very']['very']['very']['very']['deep']['deep']['end'])
        );

        $this->assertFalse(isset($p['error:not_exists']));
        $this->assertFalse(isset($p['staging:testing']));

        $this->assertEquals('127.0.0.1', $p['development']['database']['db1']['host']);
        $this->assertEquals(
                $p['production']['database']['db1']['dbName'],
                $p['development']['database']['db1']['dbName']
        );
    }

    public function testStringify() {
        $ini1      = new IniParser(self::$ini);
        $ini2      = new IniParser($ini1->toArray());
        $iniString = $ini2->toString();
        $patterns  = array(
                '[production]',
                '[testing]',
                '[development]',
                '[error]',
                '[error_2]',
                '[twoInherits]',
                '[staging]',
                'database.db1.host = error-server',
                'this.is.the.very.very.very.very.deep.deep.end = 1',
                'this.is.the.very.very.very.very.deep.end = 2',
                'this.is.the.very.very.very.very.end.2 = 2',
                'database.db1.host = test-server',
                'database.db1.dbName = dbName-prod',
                'testKey = 15'
        );

        foreach ($patterns as $pattern) {
            $this->assertTrue(!!preg_match('~' . $pattern . '~', $iniString));
        }
    }
}
