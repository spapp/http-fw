<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.05.20.
 */
namespace Test\Sky;

use PHPUnit_Framework_TestCase as TestCase;
use Sky\Parser\FormData;

/**
 * Class FormDataTest
 */
class FormDataTest extends TestCase {
    protected static $boundary = 'boundary0000008348438548954767865476786';
    protected static $params   = array(
            'text'   => 'text default',
            'number' => '11',
            'data'   => array(
                    'a' => 'a1',
                    'b' => 1
            ),
            'file1'  => 'Content of a.txt.'
    );

    protected static $content = <<<CONTENT
--boundary0000008348438548954767865476786
Content-Disposition: form-data; name="text"

text default
--boundary0000008348438548954767865476786
Content-Disposition: form-data; name="number"

11
--boundary0000008348438548954767865476786
Content-Disposition: form-data; name="data"
Content-Type: application/json

{"a":"a1","b":1}
--boundary0000008348438548954767865476786
Content-Disposition: form-data; name="file1"

Content of a.txt.

--boundary0000008348438548954767865476786--
CONTENT;

    public function testParse() {
        $p = new FormData(self::$content, self::$boundary);

        $params = $p->toArray();

        $this->assertTrue(isset($params['text']));
        $this->assertEquals(self::$params['text'], $params['text']);

        $this->assertTrue(isset($params['number']));
        $this->assertEquals(self::$params['number'], $params['number']);

        $this->assertTrue(isset($params['file1']));
        $this->assertEquals(self::$params['file1'], $params['file1']);

        $this->assertTrue(isset($params['data']));
        $this->assertTrue(is_array($params['data']));
        $this->assertEquals(self::$params['data']['a'], $params['data']['a']);
        $this->assertEquals(self::$params['data']['b'], $params['data']['b']);
    }

    public function testStringify() {
        $p = new FormData(self::$params, self::$boundary);

        $this->assertEquals(str_replace("\n", "\r\n", self::$content), $p->toString());
    }
}
