<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.05.20.
 */
namespace Test\Sky;

use PHPUnit_Framework_TestCase as TestCase;

use Sky\Parser\FormUrlencoded;

/**
 * Class FormUrlencodedTest
 */
class FormUrlencodedTest extends TestCase {
    protected static $dataArray1 = array(
            'a' => 'úűáőóüóü,',
            'b' => 'úűá       ő×óü,',
            'c' => 10,
            'd' => 12.2,
            'e' => array(
                    1,
                    2
            )
    );

    protected static $dataArray2 = array(
            'a' => 10,
            'b' => 12.2,
            'c' => array(
                    'd'=>1,
                    'e'=>2
            )
    );

    protected static $dataString1 ='a=%C3%BA%C5%B1%C3%A1%C5%91%C3%B3%C3%BC%C3%B3%C3%BC%2C&b=%C3%BA%C5%B1%C3%A1+++++++%C5%91%C3%97%C3%B3%C3%BC%2C&c=10&d=12.2&e%5B0%5D=1&e%5B1%5D=2';
    protected static $dataString2 ='a=10&b=12.2&c%5Bd%5D=1&c%5Be%5D=2';
    protected static $dataString2a ='a=10&b=12.2&c[d]=1&c[e]=2';

    public function testParse() {
        $p = new FormUrlencoded(self::$dataString1);
        $array=$p->toArray();

        $this->assertTrue(is_array($array));
        $this->assertTrue(isset($array['a']));
        $this->assertEquals(self::$dataArray1['a'],$array['a']);

        $this->assertTrue(isset($array['b']));
        $this->assertEquals(self::$dataArray1['b'],$array['b']);

        $this->assertTrue(isset($array['c']));
        $this->assertEquals(self::$dataArray1['c'],$array['c']);

        $this->assertTrue(isset($array['e']));
        $this->assertTrue(is_array($array['e']));
        $this->assertEquals(self::$dataArray1['e'][0],$array['e'][0]);


        $this->parseTest(self::$dataString2, self::$dataArray2);
        $this->parseTest(self::$dataString2a, self::$dataArray2);
    }

    private function parseTest($string, $array) {
        $p = new FormUrlencoded($string);
        $pArray=$p->toArray();

        $this->assertTrue(is_array($pArray));
        $this->assertTrue(isset($pArray['a']));
        $this->assertEquals($array['a'],$pArray['a']);

        $this->assertTrue(isset($pArray['b']));
        $this->assertEquals($array['b'],$pArray['b']);

        $this->assertTrue(isset($pArray['c']));
        $this->assertEquals($array['c'],$pArray['c']);

        $this->assertTrue(isset($pArray['c']));
        $this->assertTrue(is_array($pArray['c']));
        $this->assertEquals($array['c']['d'],$pArray['c']['d']);
        $this->assertEquals($array['c']['e'],$pArray['c']['e']);
    }

    public function testStringify() {
        $p1 = new FormUrlencoded(self::$dataArray1);
        $p2 = new FormUrlencoded(self::$dataArray2);

        $this->assertEquals(self::$dataString1, $p1->toString());
        $this->assertEquals(self::$dataString2, $p2->toString());
    }
}
