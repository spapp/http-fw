<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.06.03.
 */

namespace Test\Sky;

use PHPUnit_Framework_TestCase as TestCase;
use Sky\Uri;

/**
 * Class UriTest
 */
class UriTest extends TestCase {

    protected $uri      = 'foo://username:password@example.com:8042/over/there/index.dtb?type=animal&name=narwhal#nose';
    protected $uriArray = array(
            'scheme'   => 'foo',
            'host'     => 'example.com',
            'port'     => 8042,
            'user'     => 'username',
            'pass'     => 'password',
            'path'     => '/over/there/index.dtb',
            'query'    => 'type=animal&name=narwhal',
            'fragment' => 'nose'
    );

    protected $query      = 'type=animal&name=narwhal&x=3';
    protected $queryArray = array(
            'type' => 'animal',
            'name' => 'narwhal',
                            'x'=>3
    );

    public function testCreateFromString() {
        $uri = Uri::create($this->uri);

        foreach ($this->uriArray as $key => $value) {
            $fn = 'get' . ucfirst($key);
            $this->assertEquals($value, $uri->{$fn}());
        }

        $this->assertEquals($this->uri, $uri->toString());
    }

    public function testCreateFromArray() {
        $uri = Uri::create($this->uriArray);

        $this->assertEquals($this->uri, $uri->toString());
    }

    public function testCreateBlank() {
        $uri = Uri::create();

        $this->assertEquals('/', $uri->toString());
    }

    public function testSetQueryFromString() {
        $uri = Uri::create();
        $uri->setQuery($this->query);

        $this->setQueryTest($uri);
    }

    public function testSetQueryFromArray() {
        $uri = Uri::create();
        $uri->setQuery($this->queryArray);

        $this->setQueryTest($uri);
    }

    public function testSetPort() {
        $uri = Uri::create($this->uri);

        $this->assertEquals($this->uriArray['port'], $uri->getPort());
        $this->assertTrue(is_int($uri->getPort()));

        $uri->setPort('123');
        $this->assertEquals(123, $uri->getPort());
        $this->assertTrue(is_int($uri->getPort()));
        $this->assertFalse(is_string($uri->getPort()));
    }

    private function setQueryTest(Uri $uri){
        $this->assertEquals('/?'.$this->query, $uri->toString());

        foreach($uri->getQueryAsArray() as $k=>$v){
            $this->assertTrue(array_key_exists($k, $this->queryArray));
            $this->assertEquals($this->queryArray[$k], $v);
        }
    }

}
