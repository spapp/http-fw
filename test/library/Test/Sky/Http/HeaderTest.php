<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.05.19.
 */

namespace Test\Sky;

use PHPUnit_Framework_TestCase as TestCase;
use Sky\Http\Header;

/**
 * Class HeaderTest
 */
class HeaderTest extends TestCase {
    protected static $headers = array(
            array(
                // field name
                    'Accept',

                // field value
                    'text/*, text/html, text/html;level=1, */*',

                // best value (if applicable)
                    'text/html',

                // attribute name of best vaue
                    'level',

                // attribute value
                    1
            ),
            array(
                    'accept',
                    'text/*;q=0.3, text/html;level=3;q=0.7,text/html;q=0.7, text/html;level=1, text/html;level=2;q=0.4, */*;q=0.5',
                    'text/html',
                    'level',
                    1
            ),
            array(
                    'content-type',
                    'text/html; charset=UTF-8',
                    'text/html',
                    'charset',
                    'UTF-8'
            ),
            array(
                    'content-type',
                    'multipart/form-data; boundary=AaB03x',
                    'multipart/form-data',
                    'boundary',
                    'AaB03x'
            ),
            array(
                    'Host',
                    'example.com'
            ),
            array(
                    'User-Agent',
                    'BrowserForDummies/4.67b'
            )
    );

    /**
     * @expectedException Sky\Http\Header\Exception
     * @expectedExceptionCode 10
     */
    public function testInvalidConfigDataException() {
        new Header(self::$headers[0][1]);
    }

    public function testOneParams() {
        foreach (self::$headers as &$header) {
            $h = new Header($header[0] . ': ' . $header[1]);

            $this->assertEquals(trim($header[1]), $h->getValue());
        }
    }

    public function testTwoParams() {
        foreach (self::$headers as &$header) {
            $h = new Header($header[0], $header[1]);

            $this->assertEquals(trim($header[1]), $h->getValue());
        }
    }

    public function testToString() {
        foreach (self::$headers as &$header) {
            $h = new Header($header[0], $header[1]);

            $raw = str_replace(
                    ' ',
                    '',
                    strtolower(
                            sprintf('%s%s %s', $header[0], Header::GLUE_FIELD_PARTS, $header[1])
                    )
            );

            $rawH = str_replace(
                    ' ',
                    '',
                    strtolower(
                            $h->toString()
                    )
            );

            $this->assertEquals($raw, $rawH);
        }
    }

    public function testBestValueAndAttribute() {
        foreach (self::$headers as &$header) {
            $h = new Header($header[0], $header[1]);

            if(isset($header[2])){
                // best value
                $this->assertEquals($header[2], $h->getBestValue());
                // best attribute
                $this->assertEquals($header[4], $h->getBestAttr($header[3]));
            }
        }
    }
}
