<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.06.03.
 */

namespace Test\Sky;

use PHPUnit_Framework_TestCase as TestCase;
use Sky\Http;
use Sky\Http\Header;
use Sky\Http\Request;

/**
 * Class HeaderTest
 */
class RequestTest extends TestCase {

    protected static $requestGet = <<<GET
GET /test?x=1&y=Y HTTP/1.1
Content-Type: application/x-www-form-urlencoded
Host: localhost
User-Agent: Sky/1.0


GET;

    protected static $requestPost = <<<POST
POST /test HTTP/1.1
Content-Type: application/x-www-form-urlencoded;charset=UTF-8
Host: localhost
User-Agent: Sky/1.0

x=1&y=Y
POST;

    public function testGetRequest() {
        $request = new Request(Http::METHOD_GET, '/test');

        $request->getUri()->setHost('localhost')->setQuery(array(
                                                                   'x' => 1,
                                                                   'y' => 'Y'
                                                           ));
        $request->getHeaders()->set(Http::HEADER_CONTENT_TYPE, Http::MIME_FORM_URLENCODED);

        $this->assertEquals(self::$requestGet, str_replace("\r", '', (string)$request));
    }

    public function testPostRequest() {
        $request = new Request(Http::METHOD_POST, '/test');
        $contentType = Http::MIME_FORM_URLENCODED . ';charset=UTF-8';

        $request->setBody('x=1&y=Y')->getUri()->setHost('localhost');
        $request->getHeaders()->set(Http::HEADER_CONTENT_TYPE, $contentType);

        $this->assertEquals(self::$requestPost, str_replace("\r", '', (string)$request));
    }
}
