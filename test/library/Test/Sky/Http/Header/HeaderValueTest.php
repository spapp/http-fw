<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.05.28.
 */

namespace Test\Sky;

use PHPUnit_Framework_TestCase as TestCase;
use Sky\Http\Header\HeaderValue;

/**
 * Class HeaderValueTest
 */
class HeaderValueTest extends TestCase {
    protected static $acceptHeaderRawValue;
    protected static $contentTypeHeaderRawValue;

    public static function setUpBeforeClass() {
        self::$acceptHeaderRawValue = '*/*;level=2;q=0.8, '
                                . 'text/*;q=0.3, '
                                . 'text/html;level=3;q=0.7,'
                                . 'text/html;q=0.7, '
                                . 'text/html;level=1, '
                                . 'text/html;level=2;q=0.4, '
                                . '*/*;q=0.5';


        self::$contentTypeHeaderRawValue='multipart/form-data; boundary=AaB03x';
    }

    public function testAcceptValue() {
        $headerValue = new HeaderValue(self::$acceptHeaderRawValue);
        $valueArray  = $headerValue->toArray();

        // 1. the best value
        $this->assertEquals('text/html', $valueArray[0]['name']);
        $this->assertEquals('1', $valueArray[0]['params']['level']);
        $this->assertFalse(isset($valueArray[0]['params']['q']));

        // 2. value
        $this->assertEquals('text/html', $valueArray[1]['name']);
        $this->assertEquals('0.7', $valueArray[1]['params']['q']);
        $this->assertFalse(isset($valueArray[1]['params']['level']));

        // 3. value
        $this->assertEquals('text/*', $valueArray[2]['name']);
        $this->assertEquals('0.3', $valueArray[2]['params']['q']);
        $this->assertFalse(isset($valueArray[2]['params']['level']));

        // 4. value
        $this->assertEquals('*/*', $valueArray[3]['name']);
        $this->assertEquals('0.5', $valueArray[3]['params']['q']);
        $this->assertFalse(isset($valueArray[3]['params']['level']));

        // 5. value
        $this->assertEquals('text/html', $valueArray[4]['name']);
        $this->assertEquals('0.4', $valueArray[4]['params']['q']);
        $this->assertEquals(2, $valueArray[4]['params']['level']);

        // 6. value
        $this->assertEquals('*/*', $valueArray[5]['name']);
        $this->assertEquals('0.8', $valueArray[5]['params']['q']);
        $this->assertEquals(2, $valueArray[5]['params']['level']);

        // 7. value
        $this->assertEquals('text/html', $valueArray[6]['name']);
        $this->assertEquals('0.7', $valueArray[6]['params']['q']);
        $this->assertEquals(3, $valueArray[6]['params']['level']);
    }

    public function testContentTypeValue() {
        $headerValue = new HeaderValue(self::$contentTypeHeaderRawValue);
        $valueArray  = $headerValue->toArray();

        $this->assertEquals('multipart/form-data', $valueArray[0]['name']);
        $this->assertEquals('AaB03x', $valueArray[0]['params']['boundary']);

        $this->assertEquals(1, count($valueArray));
    }
}
