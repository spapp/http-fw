<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.06.03.
 */

namespace Test\Sky\Http\Message;

use PHPUnit_Framework_TestCase as TestCase;
use Sky\Http\Header;
use Sky\Http\Message\MessageHeaders;

/**
 * Class MessageHeadersTest
 */
class MessageHeadersTest extends TestCase {
    protected $headers = array(
            array(
                    'content-type',
                    'text/html; charset=UTF-8'
            ),
            array(
                    'Host',
                    'example.com'
            ),
            array(
                    'User-Agent',
                    'BrowserForDummies/4.67b'
            )
    );

    protected $headersText = <<< HEADERS
Content-Type: text/html; charset=UTF-8
Host: example.com
User-Agent: BrowserForDummies/4.67b
HEADERS;

    /**
     * @expectedException \Exception
     */
    public function testSetException() {
        $headers = new MessageHeaders();
        $headers->set(null);
    }

    public function testSetHeader() {
        $headers = new MessageHeaders();

        foreach ($this->headers as &$header) {
            $headers->set(new Header($header[0], $header[1]));
        }

        $headersText = str_replace("\r", '', $headers->toString());

        $this->assertEquals($this->headersText, $headersText);
    }
}
