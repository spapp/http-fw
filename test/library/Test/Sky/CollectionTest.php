<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.05.19.
 */

namespace Test\Sky;

use PHPUnit_Framework_TestCase as TestCase;
use Sky\Collection;

/**
 * Class CollectionTest
 */
class CollectionTest extends TestCase {
    protected static $data = array(
            'a' => 1,
            'b' => 'b',
            'c' => array()
    );

    protected static $collection;

    public static function setUpBeforeClass() {
        self::$collection = new Collection(self::$data);
    }

    public function testIterator() {
        foreach (self::$collection as $key => $value) {
            if ('c' === $key) {
                $this->assertFalse(is_array($value));
                $this->assertTrue(($value instanceof Collection));
            } else {
                $this->assertEquals(self::$data[$key], $value);
            }
        }
    }

    public function testCountable() {
        $this->assertTrue((count(self::$collection) === count(self::$data)));
    }

    public function testGetterSetter() {
        $this->assertTrue(self::$collection->has('a'));
        $this->assertTrue(self::$collection->hasA());

        $this->assertTrue(self::$collection->has('b'));
        $this->assertTrue(self::$collection->hasB());

        $this->assertTrue(self::$collection->has('c'));
        $this->assertTrue(self::$collection->hasC());

        $this->assertEquals(self::$data['a'], self::$collection->get('a'));
        $this->assertEquals(self::$data['a'], self::$collection->getA());

        $this->assertEquals(self::$data['b'], self::$collection->get('b'));
        $this->assertEquals(self::$data['b'], self::$collection->getB());

        $this->assertTrue((self::$collection->get('c') instanceof Collection));
        $this->assertTrue((self::$collection->getC() instanceof Collection));
    }

    public function testRemove() {
        $this->assertTrue(self::$collection->has('a'));
        $this->assertEquals(self::$collection->get('a'), self::$collection->remove('a'));
        $this->assertFalse(self::$collection->has('a'));

        $this->assertTrue(self::$collection->hasB());
        $this->assertEquals(self::$collection->getB(), self::$collection->removeB());
        $this->assertFalse(self::$collection->hasB());
    }
}