<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.05.19.
 */

namespace Test\Sky;

use PHPUnit_Framework_TestCase as TestCase;
use Sky\Config;

/**
 * Class ConfigTest
 */
class ConfigTest extends TestCase {
    protected static $resources;
    protected static $config;
    protected static $env = 'development';

    public static function setUpBeforeClass() {
        self::$resources = APPLICATION_PATH . '/test/resources';
    }

    /**
     * @expectedException Sky\Config\Exception
     * @expectedExceptionCode 10
     */
    public function testFileNotExistException() {
        $config = Config::factory(self::$resources . '/config.file', self::$env);
    }

    /**
     * @expectedException Sky\Config\Exception
     * @expectedExceptionCode 20
     */
    public function testNotSupportedTypeException() {
        $config = Config::factory(self::$resources . '/config.yaml', self::$env);
    }

    /**
     * @expectedException Sky\Config\Exception
     * @expectedExceptionCode 30
     */
    public function testInvalidConfigDataException() {
        $config = Config::factory(self::$resources . '/config.php', self::$env);
    }

    /**
     * @expectedException Sky\Config\Exception
     * @expectedExceptionCode 40
     */
    public function testInvalidEvironmentException() {
        $config = Config::factory(self::$resources . '/empty-config.ini', self::$env);
    }

    public function testFactory() {
        self::$config = Config::factory(self::$resources . '/config.ini', self::$env);
    }

    /**
     * @depends testFactory
     */
    public function testGetterSetter() {
        $dbName = self::$config->getDatabase()->get('db1')->getDbName();
        $host   = self::$config->getDatabase()->getDb1()->get('host');
        $user   = self::$config->getDatabase()->getDb1()->getUser();

        $this->assertEquals('dbName', $dbName);
        $this->assertEquals('development-server', $host);
        $this->assertEquals('dev-username', $user);
    }
}
