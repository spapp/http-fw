<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.05.22.
 */

namespace Test\Sky;

use PHPUnit_Framework_TestCase as TestCase;
use Sky\Template;

/**
 * Class TemplateTest
 */
class TemplateTest extends TestCase {

    protected $initParams = array(
            'title' => 'template title'
    );

    protected $linkParams = array(
            '/link_0' => 'label 0',
            '/link_1' => 'label 1',
            '/link_2' => 'label 2',
            '/link_3' => 'label 3',
            '/link_4' => 'label 4',
    );

    protected $params = array(
            'header' => '<i>Test</i>                  header',
            'footer' => "<pre><i>Test</i>\n\n\n            footer</pre>"
    );

    public function testConfig() {
        $t = new Template();

        $this->assertEquals(null, $t->getPath());
        $this->assertEquals('phtml', $t->getSuffix());

        $t->setConfig(
                array(
                        'path'   => 'templates/path',
                        'suffix' => 'html'
                )
        );

        $this->assertEquals('templates/path', $t->getPath());
        $this->assertEquals('html', $t->getSuffix());
    }

    public function testSetterGetter() {
        $t = new Template('templates/path', $this->initParams);

        foreach ($this->params as $name => $value) {
            $t->set($name, $value);

            $this->assertTrue($t->has($name));
            $this->assertEquals($value, $t->get($name));
        }

        $this->assertEquals($t->get('header'), $t->getHeader());
        $this->assertEquals($t->get('footer'), $t->getFooter());
        $this->assertEquals($t->get('title'), $t->getTitle());

        $this->assertFalse($t->has('not-exists'));

        $this->assertEquals($t->get('header'), $t->remove('header'));
        $this->assertFalse($t->has('header'));
    }

    public function testRender() {
        $templatePath = APPLICATION_PATH . '/test/resources';
        $t            = new Template('template/layout', $this->initParams);

        $t->addParams($this->params)
          ->set('links', $this->linkParams)
          ->setPath($templatePath);

        $html = file_get_contents($templatePath.'/template/template-output.html');
        $this->assertEquals($html, $t->render());
    }
}
