<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.05.19.
 */

define('APPLICATION_PATH', dirname(dirname(__FILE__)));

require(APPLICATION_PATH . '/library/Autoloader.php');

Autoloader::getInstance()
          ->addIncludePath(
                  array(
                          APPLICATION_PATH . '/library',
                          APPLICATION_PATH . '/test/library'
                  )
          )
          ->register();

