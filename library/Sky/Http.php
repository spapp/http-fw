<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.05.26.
 */

namespace Sky;

/**
 * Class Http
 */
class Http {

    const EOL = "\r\n";

    const PROTOCOL = 'HTTP';

    const SCHEME      = 'http';
    const SCHEME_SAFE = 'https';
    const PORT        = 80;
    const PORT_SAFE   = 443;

    const HEADER_CONTENT_TYPE = 'Content-Type';
    const HEADER_HOST         = 'Host';
    const HEADER_USER_AGENT   = 'User-Agent';

    const MIME_TEXT            = 'text/plain';
    const MIME_JSON            = 'application/json';
    const MIME_FORM_URLENCODED = 'application/x-www-form-urlencoded';
    const MIME_FORM_DATA       = 'multipart/form-data';

    const METHOD_OPTIONS = 'OPTIONS';
    const METHOD_GET     = 'GET';
    const METHOD_HEAD    = 'HEAD';
    const METHOD_POST    = 'POST';
    const METHOD_PUT     = 'PUT';
    const METHOD_DELETE  = 'DELETE';
    const METHOD_TRACE   = 'TRACE';
    const METHOD_CONNECT = 'CONNECT';

    /**
     * Constructor
     */
    final private function __construct() {

    }
}
