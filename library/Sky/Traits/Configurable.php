<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.05.26.
 */

namespace Sky\Traits;

use Sky\Collection;

trait Configurable {
    /**
     * @param array|Collection $aConfig
     *
     * @return void
     * @throws \Exception
     */
    public function setConfig($aConfig) {
        if ($aConfig instanceof Collection) {
            $aConfig = $aConfig->toArray();
        }

        if (!is_array($aConfig)) {
            throw new \Exception('Not supported type');
        }

        foreach ($aConfig as $name => $value) {
            $method = 'set' . ucfirst($name);

            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }
}
