<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.05.20.
 */

namespace Sky\Parser;

/**
 * Class FormUrlencoded
 */
class FormUrlencoded extends AbstractParser {
    /**
     * @var string
     */
    private $argSeparator = '&';
    /**
     * @var int
     */
    private $encodingType = PHP_QUERY_RFC1738;

    /**
     * Constructor
     *
     * @param array|string $aData
     * @param null|string  $argSeparator
     * @param null|int     $encodingType
     */
    public function __construct($aData = null, $argSeparator = null, $encodingType = null) {
        parent::__construct($aData);

        if (null !== $argSeparator) {
            $this->argSeparator = $argSeparator;
        }

        if (null !== $encodingType) {
            $this->encodingType = $encodingType;
        }
    }

    /**
     * @inheritdoc
     * @return array
     */
    protected function parse() {
        parse_str($this->dataString, $return);

        return $return;
    }

    /**
     * @inheritdoc
     * @return string
     */
    protected function stringify() {
        return http_build_query($this->dataArray, null, $this->argSeparator, $this->encodingType);
    }
}
