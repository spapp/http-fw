<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.05.20.
 */

namespace Sky\Parser;

/**
 * Class Ini
 */
class Ini extends AbstractParser {

    const SEPARATOR_EXTENDS = ':';
    const SEPARATOR_KEY     = '.';
    const TEMPLATE_SECTION  = '[%s]';
    const TEMPLATE_PROPERTY = '%s = %s';

    /**
     * Parses the string
     * Returns the array representation of the string data
     *
     * @return array
     */
    protected function parse() {
        $data = $this->parseSections(parse_ini_string(trim($this->dataString), true));

        foreach ($data as $key => &$value) {
            $this->parseKey($value);
        }

        return $data;
    }

    /**
     * Returns the string representation of the data
     *
     * @return string
     */
    protected function stringify() {
        $return = array();

        foreach ($this->dataArray as $section => &$sectionValue) {
            array_push($return, sprintf(self::TEMPLATE_SECTION, $section));
            array_push($return, $this->stringifySection($sectionValue), '');
        }

        return implode(PHP_EOL, $return);
    }

    /**
     * Returns the string representation of a section data
     *
     * @param array $aValue
     * @param array $aParentKeys
     *
     * @return string
     */
    private function stringifySection(array $aValue, array $aParentKeys = array()) {
        $return = array();

        foreach ($aValue as $key => $value) {
            $propertyKey = array_merge((array)$aParentKeys, (array)$key);

            if (is_array($value)) {
                array_push($return, $this->stringifySection($value, $propertyKey));
            } else {
                array_push(
                        $return,
                        sprintf(
                                self::TEMPLATE_PROPERTY,
                                implode(self::SEPARATOR_KEY, $propertyKey),
                                $value
                        )
                );
            }
        }

        return implode(PHP_EOL, $return);
    }

    /**
     * Parses an arry to multidimensional array
     *
     * @param array &$aData
     *
     * @return void
     */
    private function parseKey(&$aData) {
        foreach ($aData as $key => &$value) {
            if (strpos($key, self::SEPARATOR_KEY) !== false) {
                $subKeys = explode(self::SEPARATOR_KEY, $key);
                $lastKey = array_pop($subKeys);
                $current = &$aData;

                foreach ($subKeys as $subKey) {
                    if (!isset($current[$subKey])) {
                        $current[$subKey] = array();
                    }

                    $current = &$current[$subKey];
                }

                $current[$lastKey] = $value;

                unset($aData[$key]);
            }
        }
    }

    /**
     * Parses an array with inheritance
     *
     * @param array $aData
     *
     * @return array
     */
    private function parseSections($aData) {
        $sections = array_keys($aData);
        $response = array();

        while (count($sections) > 0) {
            $section = array_shift($sections);
            $first   = true;

            if (is_array($section) or false !== strpos($section, self::SEPARATOR_EXTENDS)) {
                if (is_array($section)) {
                    $parts   = $section['parts'];
                    $section = $section['section'];
                    $first   = false;
                } else {
                    $parts = explode(self::SEPARATOR_EXTENDS, $section, 2);
                }

                $sectionName   = trim($parts[0]);
                $parentSection = isset($parts[1]) ? trim($parts[1]) : '';

                if ($parentSection) {
                    if (isset($response[$parentSection])) {
                        $response[$sectionName] = array_merge($response[$parentSection], $aData[$section]);
                    } elseif (true === $first) {
                        array_push($sections,
                                   array(
                                           'parts'   => $parts,
                                           'section' => $section
                                   )
                        );
                    } else {
                        $response[$sectionName] = $aData[$section];
                    }
                } else {
                    $response[$sectionName] = $aData[$section];
                }

            } else {
                $response[trim($section)] = $aData[$section];
            }
        }

        return $response;
    }
}
