<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.05.20.
 */

namespace Sky\Parser;

/**
 * Class AbstractParser
 */
abstract class  AbstractParser {
    /**
     * @var array
     */
    protected $dataArray = null;
    /**
     * @var string
     */
    protected $dataString = null;

    /**
     * @param array|string $aData
     */
    public function __construct($aData = null) {
        $this->setData($aData);
    }

    /**
     * Returns the string representation of the data
     *
     * @return string
     */
    public final function toString() {
        if (null === $this->dataString) {
            $this->dataString = $this->stringify();
        }

        return $this->dataString;
    }

    /**
     * Returns the array representation of the data
     *
     * @return array
     */
    public final function toArray() {
        if (null === $this->dataArray) {
            $this->dataArray = $this->parse();
        }

        return $this->dataArray;
    }

    /**
     * Initializes the parser data
     *
     * @param string|array $aData
     *
     * @return $this
     */
    public final function setData($aData) {
        if ('string' === gettype($aData)) {
            $this->dataString = $aData;
            $this->dataArray  = null;
        } elseif (is_array($aData)) {
            $this->dataString = null;
            $this->dataArray  = $aData;
        } else {
            $this->dataString = null;
            $this->dataArray  = null;
        }

        return $this;
    }

    /**
     * Magice function
     *
     * @return string
     */
    public final function __toString() {
        return $this->toString();
    }

    /**
     * Parses the string
     * Returns the array representation of the string data
     *
     * @return array
     */
    protected abstract function parse();

    /**
     * Returns the string representation of the data
     *
     * @return string
     */
    protected abstract function stringify();
}
