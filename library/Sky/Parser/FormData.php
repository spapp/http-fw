<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.05.20.
 */

namespace Sky\Parser;

/**
 * Class FormData
 */
class FormData extends AbstractParser {
    /**
     * @const
     */
    const PREFIX_BOUNDARY = '--';
    /**
     * @const
     */
    const EOL = "\r\n";
    /**
     * @var int
     * @static
     */
    private static $index = 0;
    /**
     * @var string
     */
    private $boundary;

    /**
     * Constructor
     *
     * @param array|string $aData
     * @param null|string  $argSeparator
     * @param null|int     $encodingType
     */
    public function __construct($aData = null, $boundary = null) {
        parent::__construct($aData);

        if (null !== $boundary) {
            $this->boundary = $boundary;
        }
    }

    /**
     * Generates a boundary string
     *
     * @return null|string
     */
    public function getBoundary() {
        if (isset($this->boundary)) {
            return $this->boundary;
        }

        return md5(mktime() . '-' . (++self::$index));
    }

    /**
     * @inheritdoc
     * @return array
     */
    protected function parse() {
        $data        = explode(self::PREFIX_BOUNDARY . $this->getBoundary(), trim($this->dataString));
        $namePattern = '~^Content-Disposition.*form-data.*name="([a-z_][a-z0-9_-]+)"~i';
        $params      = array();

        foreach ($data as $block) {
            $block       = explode("\n", trim($block));
            $name        = '';
            $contentType = '';
            $value       = '';

            foreach ($block as $line) {
                $line = trim(str_replace("\r", '', $line));

                if (!$line or self::PREFIX_BOUNDARY === $line) {
                    continue;
                }

                if (preg_match($namePattern, $line, $matches) and isset($matches[1])) {
                    $name = $matches[1];
                } elseif (preg_match('~Content-Type~', $line)) {
                    $contentType = explode(':', $line);
                    $contentType = trim($contentType[1]);
                } else {
                    $value .= $line;
                }
            }

            if ('application/json' === $contentType) {
                $value = json_decode($value, true);
            }

            if ($name) {
                $params[$name] = $value;
            }
        }

        return $params;
    }

    /**
     * @inheritdoc
     * @return string
     */
    protected function stringify() {
        $return      = array();
        $disposition = 'Content-Disposition: form-data; name="%s"';
        $contentType = 'Content-Type: application/json';
        $boundary    = $this->getBoundary();

        foreach ($this->dataArray as $name => $value) {
            array_push(
                    $return,
                    self::PREFIX_BOUNDARY,
                    $boundary,
                    self::EOL,
                    sprintf($disposition, $name),
                    self::EOL
            );

            if (is_array($value)) {
                array_push(
                        $return,
                        $contentType,
                        self::EOL

                );

                $value = json_encode($value);
            }

            array_push($return, self::EOL, $value, self::EOL);
        }

        array_push($return, self::EOL, self::PREFIX_BOUNDARY, $boundary, self::PREFIX_BOUNDARY);

        return implode('', $return);
    }
}
