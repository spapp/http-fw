<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.05.22.
 */

namespace Sky;

use Sky\Template\Exception as TemplateException;
use Sky\Traits\Configurable;
use Sky\Traits\ConfigurableInterface;

/**
 * Class Template
 *
 * @example config
 *
 * array (
 *      'path'           => '',
 *      'suffix'         => 'phtml',
 *      'template'       => '',
 *      'minimize'       => true,
 *      'withoutComment' => true
 * );
 */
class Template implements ConfigurableInterface {

    use Configurable;

    const PATTERN_COMMENT  = '~<!--(.|\s)*?-->~';
    const PATTERN_MINIMIZE = '~(?>[^\S ]\s*|\s{2,})(?=(?:(?:[^<]++|<(?!/?(?:textarea|pre)\b))*+)(?:<(?>textarea|pre)\b|\z))~ix';

    /**
     * @var Collection
     */
    protected $params = null;
    /**
     * @var string
     */
    protected $path = null;
    /**
     * @var string
     */
    protected $suffix = 'phtml';

    /**
     * @var string
     */
    protected $template = null;
    /**
     * @var bool
     */
    protected $minimize = true;
    /**
     * @var bool
     */
    protected $withoutComment = true;

    /**
     * Constructor
     *
     * @param string $aTemplateFile
     * @param array  $aParams
     */
    public function __construct($aTemplateFile = null, $aParams = array()) {
        $this->params = new Collection($aParams);
        $this->setTemplate($aTemplateFile);
    }

    /**
     * Magice method
     *
     * Allows access to the internal params collection.
     *
     * @param string $aName
     * @param array  $aArgs
     *
     * @return mixed|Template
     * @throws TemplateException
     */
    public function __call($aName, $aArgs) {
        $action = strtolower(substr($aName, 0, 3));
        $allows = array(
                'get',
                'set',
                'has'
        );

        if (in_array($action, $allows) or 'remove' === strtolower(substr($aName, 0, 6))) {
            $return = call_user_func_array(
                    array(
                            $this->params,
                            $aName
                    ),
                    $aArgs
            );

            if ('set' === strtolower(substr($aName, 0, 3))) {
                $return = $this;
            }
        } else {
            throw new TemplateException('Not supported method');
        }

        return $return;
    }

    /**
     * Returns a template variable
     *
     * If the $aEscape is TRUE then converts all applicable characters to HTML entities
     *
     * @param string $aName
     * @param mixed  $aDefault
     * @param bool   $aEscape
     *
     * @return mixed
     */
    public function get($aName, $aDefault = null, $aEscape = false) {
        $value = $this->params->get($aName, $aDefault);

        if (true === $aEscape) {
            $value = htmlentities($value, ENT_COMPAT);
        }

        return $value;
    }

    /**
     * Returns TRUE if the compression is turned on
     *
     * @return boolean
     */
    public function isMinimize() {
        return $this->minimize;
    }

    /**
     * Sets the output to be minimized or not
     *
     * @param boolean $aWithoutComment
     *
     * @return $this
     */
    public function setMinimize($aMinimize) {
        $this->minimize = $aMinimize;

        return $this;
    }

    /**
     * Returns TRUE if the comments removing is turned on
     *
     * @return boolean
     */
    public function isWithoutComment() {
        return $this->withoutComment;
    }

    /**
     * Set to TRUE if you want to remove comments from the output
     *
     * @param boolean $aWithoutComment
     *
     * @return $this
     */
    public function setWithoutComment($aWithoutComment) {
        $this->withoutComment = $aWithoutComment;

        return $this;
    }

    /**
     * Returns template name
     *
     * @return string
     */
    public function getTemplate() {
        return $this->template;
    }

    /**
     * Sets current template
     *
     * @param string $aTemplate
     *
     * @return $this
     */
    public function setTemplate($aTemplate) {
        $this->template = $aTemplate;

        return $this;
    }

    /**
     * Returns template suffix
     *
     * @return string
     */
    public function getSuffix() {
        return $this->suffix;
    }

    /**
     * Sets template suffix
     *
     * @param string $aSuffix
     *
     * @return $this
     */
    public function setSuffix($aSuffix) {
        $this->suffix = $aSuffix;

        return $this;
    }

    /**
     * Returns template base path
     *
     * @return string
     */
    public function getPath() {
        return $this->path;
    }

    /**
     * Sets template base path
     *
     * @param string $aPath
     *
     * @return $this
     */
    public function setPath($aPath) {
        $this->path = $aPath;

        return $this;
    }

    /**
     * Appends template params
     *
     * @param array $aParams
     *
     * @return $this
     */
    public function addParams(array $aParams) {
        foreach ($aParams as $name => $value) {
            $this->params->set($name, $value);
        }

        return $this;
    }

    /**
     * Magice method
     */
    public function __toString() {
        return $this->render();
    }

    /**
     * Returns a rendered template as string
     *
     * @param null|string $aTemplateFile
     *
     * @return string
     */
    public function render($aTemplateFile = null) {
        if (null === $aTemplateFile) {
            $aTemplateFile = $this->getTemplate();
        }

        ob_start();

        include($this->getTemplateFullPath($aTemplateFile));


        return $this->filter(ob_get_clean());
    }

    /**
     * Returns filtered template text
     *
     * @param string $text
     *
     * @return string
     */
    protected function filter($text) {
        if (true === $this->isWithoutComment()) {
            $text = preg_replace(self::PATTERN_COMMENT, '', $text);
        }

        if (true === $this->isMinimize()) {
            $text = preg_replace(self::PATTERN_MINIMIZE, ' ', $text);
        }

        return $text;
    }

    /**
     * Returns the absolute path of template
     *
     * @param string $aTemplateFile
     *
     * @return string
     */
    protected function getTemplateFullPath($aTemplateFile) {
        if (preg_match('~^/~', $aTemplateFile)) {
            return $aTemplateFile;
        }

        $suffix = $this->getSuffix();
        $path   = $this->getPath();

        if (!preg_match('~\.' . $suffix . '$~', $aTemplateFile)) {
            $aTemplateFile .= '.' . $suffix;
        }

        return $path . DIRECTORY_SEPARATOR . $aTemplateFile;
    }
}
