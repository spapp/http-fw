<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.05.20.
 */

namespace Sky;

interface AccessibleInterface {
    /**
     * Getter method
     *
     * @param string $aName
     * @param mixed  $aDefault
     *
     * @return mixed
     */
    public function get($aName, $aDefault);

    /**
     * Setter method
     *
     * @param string $aName
     * @param mixed  $aDefault
     *
     * @return Object current instance
     */
    public function set($aName, $aValue);

    /**
     * Validator method
     *
     * @param string $aName
     *
     * @return bool
     */
    public function has($aName);

    /**
     * Removes the specified item and returns it
     *
     * @param string $aName
     *
     * @return mixed
     */
    public function remove($aName);
}
