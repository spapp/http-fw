<?php
/**
 * @author     Sándor Papp <spapp@spappsite.hu>
 * @copyright  2015
 * @license    http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link       https://bitbucket.org/spapp
 * @package    http_fw
 * @since      2015.05.19.
 */

namespace Sky;

/**
 * Class Collection
 */
class Collection implements \Iterator, \Countable, AccessibleInterface {
    /**
     * @var int
     */
    private $position = 0;
    /**
     * @var array
     */
    private $data = array();

    /**
     * Constructor
     *
     * @param array $data
     */
    public function __construct(array $data = array()) {
        $this->setData($data);
    }

    /**
     * Returns the value to which the specified key is mapped,
     * or the default if this collection contains no mapping for the key.
     * If the value is a array then returns a new collection.
     *
     * @param string $aName
     * @param null   $aDefault
     *
     * @see GetterSetter
     * @return null|Collection
     */
    public function get($aName, $aDefault = null) {
        if ($this->has($aName)) {
            $aDefault = $this->data[$aName];
        }

        if (null !== $aDefault and is_array($aDefault)) {
            $aDefault = new Collection($aDefault);
        }

        return $aDefault;
    }

    /**
     * Sets a new value for the specified key.
     *
     * @param string $aName
     * @param mixed  $aValue
     *
     * @see GetterSetter
     * @return $this
     */
    public function set($aName, $aValue) {
        $this->data[$aName] = $aValue;

        return $this;
    }

    /**
     * Returns TRUE if the specified key is exists in the collection.
     *
     * @param string $aName
     *
     * @see GetterSetter
     * @return bool
     */
    public function has($aName) {
        return array_key_exists($aName, $this->data);
    }

    /**
     * Removes the specified item and returns it
     *
     * @param string $aName
     *
     * @return null|Collection
     */
    public function remove($aName) {
        $item = null;

        if (true === $this->has($aName)) {
            $item = $this->get($aName);
            unset($this->data[$aName]);
        }

        return $item;
    }

    /**
     * Magice method
     *
     * __call() is triggered when invoking inaccessible methods in an object context.
     * Provides a setter and a getter method to all elements of the collection.
     *
     * <code>
     *      $car = new Collection(array('color'=>'red'));
     *
     *      $car->getColor(); // red
     *
     *      $car->hasMaxSpeed(); // false
     *      $car->has('maxSpeed'); // false
     *
     *      $car->setMaxSpeed(120);
     *
     *      $car->getMaxSpeed(); // 120
     *      $car->get('maxSpeed'); // 120
     * </code>
     *
     * @param string $aName method name
     * @param array  $aArgs method arguments
     *
     * @return mixed
     */
    public function __call($aName, $aArgs) {
        $action = strtolower(substr($aName, 0, 3));
        $name   = lcfirst(substr($aName, 3));
        $param  = isset($aArgs[0]) ? $aArgs[0] : null;

        if ('get' === $action) {
            return $this->get($name, $param);
        } elseif ('set' === $action) {
            return $this->set($name, $param);
        } elseif ('has' === $action) {
            return $this->has($name);
        } elseif ('remove' === strtolower(substr($aName, 0, 6))) {
            $name = lcfirst(substr($aName, 6));

            return $this->remove($name);
        }

        throw new \Exception("Not supported method: " . $aName);
    }

    /**
     * Returns collection as an array
     *
     * @return array
     */
    public function toArray() {
        return $this->data;
    }

    /**
     * Initializes the collection data
     *
     * @param array $data
     *
     * @return $this
     */
    public function setData(array $data) {
        $this->data = $data;
        $this->rewind();

        return $this;
    }

    /**
     * Returns collection item count
     *
     * @link http://php.net/manual/en/class.countable.php
     * @return int
     */
    public function count() {
        return count($this->data);
    }

    /**
     * Rewind the Iterator to the first element
     *
     * @link http://php.net/manual/en/class.iterator.php
     * @return void
     */
    public function rewind() {
        $this->position = 0;
    }

    /**
     * Returns the current element
     *
     * @link http://php.net/manual/en/class.iterator.php
     * @return null|mixed|Collection
     */
    public function current() {
        return $this->get($this->key());
    }

    /**
     * Return the key of the current element
     *
     * @link http://php.net/manual/en/class.iterator.php
     * @return mixed
     */
    public function key() {
        $keys = array_keys($this->data);

        return $keys[$this->position];
    }

    /**
     * Move forward to next element
     *
     * @link http://php.net/manual/en/class.iterator.php
     * @return void
     */
    public function next() {
        ++$this->position;
    }

    /**
     * Checks if current position is valid
     *
     * @link http://php.net/manual/en/class.iterator.php
     * @return bool
     */
    public function valid() {
        $keys = array_keys($this->data);

        return (isset($keys[$this->position]) and isset($this->data[$keys[$this->position]]));
    }
}
