<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.05.28.
 */

namespace Sky\Http\Header;

/**
 * Class HeaderValue
 *
 * Parsing and sorting (relative quality factor)
 *
 */
class HeaderValue {
    /**
     * @var array
     */
    protected $values = null;
    /**
     * @var bool
     */
    protected $sorted = false;

    /**
     * Constructor
     *
     * @param string $aFieldValue
     */
    public function __construct($aFieldValue) {
        $this->values = array();
        $values       = explode(',', $aFieldValue);

        foreach ($values as $value) {
            $this->addValue($value);
        }
    }

    /**
     * Returns the specified index value
     *
     * @param int $aIndex
     *
     * @return null|array
     */
    public function getValue($aIndex = 0) {
        if (isset($this->values[$aIndex])) {
            $this->sortValues();

            return $this->values[$aIndex];
        }

        return null;
    }

    /**
     * Returns all values and attributes as an array
     *
     * Structure of array:
     *
     * <code>
     *  <?php
     *  $header = new Header('Content-Type: text/html; charset=UTF-8');
     *
     *  print_r($header->getValues()->toArray());
     *  ?>
     *
     *  // output:
     *  array(
     *      array(
     *          'name'   => 'text/plain',
     *          'params' => array(
     *              'charset' => 'UTF-8'
     *          )
     *      )
     *  );
     *
     *  <?php
     *  $header = new Header('Accept: text/*;q=0.3,text/html;q=0.7');
     *
     *  print_r($header->getValues()->toArray());
     *  ?>
     *
     *  // output:
     *  array(
     *      array(
     *          'name'   => 'text/html',
     *          'params' => array(
     *              'q' => '0.7'
     *          )
     *      ),
     *      array(
     *          'name'   => 'text/*',
     *          'params' => array(
     *              'q' => '0.3'
     *          )
     *      )
     *  );
     * </code>
     *
     * @return array
     */
    public function toArray() {
        $this->sortValues();

        return $this->values;
    }

    /**
     * Appends a value
     *
     * @param string $aValue
     * @param array  $aParams
     *
     * @return $this
     */
    public function addValue($aValue, array $aParams = null) {
        $value = array(
                'name'   => '',
                'params' => array()
        );

        if (null === $aParams) {
            $aParams = explode(';', $aValue);

            $value['name'] = trim(array_shift($aParams));
        }

        foreach ($aParams as $param) {
            list($paramName, $paramValue) = array_map('trim',
                    (explode('=', $param, 2) + array(
                                    '',
                                    ''
                            )));

            $value['params'][$paramName] = $paramValue;
        }

        array_push($this->values, $value);

        $this->sorted = false;

        return $this;
    }

    /**
     * The comparison function
     *
     * @see HeaderValue::sortValues()
     *
     * @param array $a
     * @param array $b
     *
     * @return int
     */
    protected function valueCompare($a, $b) {
        $qualityA = isset($a['params']['q']) ? (float)$a['params']['q'] : 1.0;
        $qualityB = isset($b['params']['q']) ? (float)$b['params']['q'] : 1.0;
        $levelA   = isset($a['params']['level']) ? (int)$a['params']['level'] : 1;
        $levelB   = isset($b['params']['level']) ? (int)$b['params']['level'] : 1;

        if ('*/*' === $a['name']) {
            $qualityA = 0.01;
        } elseif ('*' === substr($a['name'], -1)) {
            $qualityA = 0.02;
        }

        if ('*/*' === $b['name']) {
            $qualityB = 0.01;
        } elseif ('*' === substr($b['name'], -1)) {
            $qualityB = 0.02;
        }

        $qualityA -= $levelA;
        $qualityB -= $levelB;

        if ($qualityA > $qualityB) {
            return -1;
        } elseif ($qualityA < $qualityB) {
            return 1;
        }

        return 0;
    }

    /**
     * Sort header values
     *
     * @see HeaderValue::valueCompare()
     * @return void
     */
    protected function sortValues() {
        if (false === $this->sorted) {
            usort($this->values,
                  array(
                          $this,
                          'valueCompare'
                  )
            );
        }

        $this->sorted = true;
    }
}
