<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.05.29.
 */

namespace Sky\Http\Message;

use Sky\Http;

/**
 * Class MessageAbstract
 */
abstract class MessageAbstract implements MessageInterface {
    /**
     * @var int|string
     */
    protected $protocolVersion = '1.1';
    /**
     * @var MessageHeaders
     */
    protected $headers;
    /**
     * @var string
     */
    protected $body;

    /**
     * Constructor
     */
    public function __construct(array $aHeaders = array(), $aBody = '') {
        $this->getHeaders()->setData($aHeaders);
        $this->setBody($aBody);
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    final public function getProtocol() {
        return sprintf('%s/%.1f', Http::PROTOCOL, $this->getProtocolVersion());
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    final public function getProtocolVersion() {
        return $this->protocolVersion;
    }

    /**
     * {@inheritdoc}
     *
     * @param int|string $aProtocolVersion
     *
     * @return $this
     */
    final public function setProtocolVersion($aProtocolVersion) {
        $this->protocolVersion = $aProtocolVersion;

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return MessageHeaders
     */
    final public function getHeaders() {
        if (null === $this->headers) {
            $this->headers = new MessageHeaders();
        }

        return $this->headers;
    }

    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public function getBody() {
        return $this->body;
    }

    /**
     * {@inheritdoc}
     *
     * @param string $aBody
     *
     * @return $this
     */
    public function setBody($aBody) {
        $this->body = $aBody;

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    final public function __toString() {
        return $this->toString();
    }

    abstract public function toString();
}
