<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.05.29.
 */

namespace Sky\Http\Message;

use Sky\Http\Message\Headers;

interface MessageInterface {
    /**
     * Returns the protocol and version as a string
     *
     * @return string
     */
    public function getProtocol();
    /**
     * Returns the protocol version
     *
     * @return string
     */
    public function getProtocolVersion();

    /**
     * Sets the protocol version
     *
     * @param int|string $aProtocolVersion
     *
     * @return mixed
     */
    public function setProtocolVersion($aProtocolVersion);

    /**
     * Returns the message headers collection
     *
     * @return Headers
     */
    public function getHeaders();

    /**
     * Returns the message body
     *
     * @return string
     */
    public function getBody();

    /**
     * Sets the message body
     *
     * @param string $aBody
     *
     * @return string
     */
    public function setBody($aBody);

    /**
     * Magice method
     *
     * Returns HTTP message as a string
     *
     * @return mixed
     */
    public function __toString();
}
