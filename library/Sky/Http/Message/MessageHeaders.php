<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.06.03.
 */

namespace Sky\Http\Message;

use Sky\Collection;
use Sky\Http;
use Sky\Http\Header;

/**
 * Class Headers
 */
class MessageHeaders extends Collection {
    /**
     * Sets a new header
     *
     * @param Header|string $aName
     * @param Header|string $aHeader
     *
     * @return $this
     */
    public function set($aName, $aHeader = null) {
        if (null === $aHeader) {
            if (is_string($aName)) {
                $aHeader = new Header($aName);
            } elseif ($aName instanceof Header) {
                $aHeader = $aName;
            }
        } elseif (!($aHeader instanceof Header)) {
            $aHeader = new Header($aName, $aHeader);
        }

        if (!($aHeader instanceof Header)) {
            throw new \Exception('Not supported type');
        }

        parent::set($aHeader->getName(), $aHeader);
    }

    /**
     * Returns headers as a string
     *
     * @return string
     */
    public function toString() {
        $headers = array();

        foreach ($this as $header) {
            array_push($headers, $header->toString());
        }

        return implode(Http::EOL, $headers);
    }

    /**
     * Magice method
     *
     * @see Headers::toString()
     *
     * @return string
     */
    public function __toString() {
        return $this->toString();
    }
}