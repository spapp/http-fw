<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.05.29.
 */

namespace Sky\Http;

use Sky\Http;
use Sky\Http\Message\MessageAbstract;
use Sky\Sky;
use Sky\Uri;

/**
 * Class Request
 */
class Request extends MessageAbstract {
    /**
     * @var string
     */
    protected $method = null;
    /**
     * @var Uri
     */
    protected $uri = null;

    /**
     * Constructor
     *
     * @param string $aMethod
     * @param string $aPath
     * @param array  $aHeaders
     * @param string $aBody
     */
    public function __construct($aMethod, $aPath = '', array $aHeaders = array(), $aBody = '') {
        $this->method = $aMethod;

        $this->getUri()->setPath($aPath);
        parent::__construct($aHeaders, $aBody);
    }

    /**
     * Returns the request method
     *
     * @return string
     */
    public function getMethod() {
        return $this->method;
    }

    /**
     * Returns the request uri
     *
     * @return Uri
     */
    public function getUri() {
        if (null === $this->uri) {
            $this->uri = new Uri();
        }

        return $this->uri;
    }

    /**
     * Returns this request as a string
     *
     * @return string
     * @throws \Exception
     */
    public function toString() {
        $this->getHeaders()->set(Http::HEADER_HOST, $this->getUri()->getHost());
        $this->getHeaders()->set(Http::HEADER_USER_AGENT, sprintf('%s/%s', Sky::VENDOR, Sky::VERSION));

        $request = array(
                sprintf('%s %s %s', $this->getMethod(), $this->getUri()->getLocalPart(), $this->getProtocol()),
                $this->getHeaders()->toString(),
                '',
                $this->getBody()
        );

        return implode(Http::EOL, $request);
    }
}