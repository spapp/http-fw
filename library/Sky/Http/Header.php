<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.05.22.
 */

namespace Sky\Http;

use Sky\Http\Header\Exception as HeaderException;
use Sky\Http\Header\HeaderValue;

/**
 * Class Header
 *
 * Represents a HTTP header field
 */
class Header {
    const GLUE_FIELD_PARTS = ':';

    /**
     * @var string
     */
    protected $name;
    /**
     * @var string
     */
    protected $value;
    /**
     * @var bool
     */
    protected $replace = true;
    /**
     * @var HeaderValue
     */
    protected $values = null;

    /**
     * Constructor
     *
     * @param string $aFieldName
     * @param string $aFieldValue
     *
     * @throws HeaderException
     */
    public function __construct($aFieldName, $aFieldValue = null) {
        if (null === $aFieldValue) {
            $aFieldName = explode(self::GLUE_FIELD_PARTS, $aFieldName, 2);

            if (count($aFieldName) === 2) {
                $aFieldValue = $aFieldName[1];
                $aFieldName  = $aFieldName[0];
            } else {
                throw new HeaderException('The raw header does not contain field name', 10);
            }
        }

        $this->setName($aFieldName);
        $this->setValue($aFieldValue);
    }

    /**
     * Returns TRUE if the header will be replaced
     *
     * By default TRUE
     *
     * @return boolean
     */
    public function isReplace() {
        return $this->replace;
    }

    /**
     * Sets the header replace a previous similar header
     *
     * By default will replace (TRUE)
     *
     * @param boolean $aReplace
     *
     * @return $this
     */
    public function setReplace($aReplace) {
        $this->replace = $aReplace;

        return $this;
    }

    /**
     * Returns the header field name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Returns the header field raw value
     *
     * @return string
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * Returns a raw HTTP header
     *
     * @return string
     */
    public function  toString() {
        return sprintf('%s%s %s', $this->getName(), self::GLUE_FIELD_PARTS, $this->getValue());
    }

    /**
     * Magice function
     *
     * @see Header::toString()
     * @return string
     */
    public function  __toString() {
        return $this->toString();
    }

    /**
     * Sends the current header
     *
     * @return bool
     */
    public function  send() {
        if (!headers_sent()) {
            header($this->toString(), $this->isReplace());

            return true;
        }

        return false;
    }

    /**
     * Returns the best header value
     *
     * @return array|null
     */
    public function getBestValue() {
        $bestValue = $this->getValues()->getValue(0);

        if (isset($bestValue) and isset($bestValue['name'])) {
            $bestValue = $bestValue['name'];
        }

        return $bestValue;

    }

    /**
     * Returns an spcified attribute value of 'best value'
     *
     * If it is not exists then returns specified default value
     *
     * @see Header::getBestValue()
     *
     * @param string $aAttributeName
     * @param null   $aDefault
     *
     * @return null
     */
    public function getBestAttr($aAttributeName, $aDefault = null) {
        $bestValue = $this->getValues()->getValue(0);

        if (isset($bestValue) and isset($bestValue['params']) and isset($bestValue['params'][$aAttributeName])) {
            $aDefault = $bestValue['params'][$aAttributeName];
        }

        return $aDefault;
    }

    /**
     * Returns header field values
     *
     * @return HeaderValue
     */
    public function getValues() {
        if (null === $this->values) {
            $this->values = new HeaderValue($this->getValue());
        }

        return $this->values;
    }

    /**
     * Sets and prepare the header field name
     *
     * @param string $aName
     *
     * @return $this
     */
    protected function setName($aName) {
        $this->name = str_replace(
                ' ',
                '-',
                ucwords(
                        str_replace(
                                array(
                                        '-',
                                        '_'
                                ),
                                ' ',
                                trim($aName)
                        )
                )
        );

        return $this;
    }

    /**
     * Sets the header field raw value
     *
     * @param string $value
     *
     * @return $this
     */
    protected function setValue($value) {
        $this->value = trim($value);

        return $this;
    }
}
