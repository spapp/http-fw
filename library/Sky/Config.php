<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.05.22.
 */

namespace Sky;

use Sky\Config\Exception as ConfigException;
use Sky\Parser\Ini as IniParser;

/**
 * Class Config
 */
class Config extends Collection {
    /**
     * @var array
     * @static
     */
    protected static $errors = array(
            10 => 'The file (%s) is not readable or does not exist',
            20 => 'Not supported type (%s)',
            30 => 'The file (%s) does not contain a valid configuration data',
            40 => 'The config does not contain "%s" environment'
    );

    /**
     * Constructor
     *
     * @param array  $data
     * @param string $environment
     *
     * @throws ConfigException
     */
    public function __construct(array $data, $environment) {
        if (array_key_exists($environment, $data)) {
            parent::__construct($data[$environment]);
        } else {
            throw new ConfigException(sprintf(self::$errors[40], $environment), 40);
        }
    }

    /**
     * Config factory
     *
     * @param string $fileName
     * @param string $environment
     *
     * @static
     * @return Config
     * @throws ConfigException
     */
    public static function factory($fileName, $environment) {
        $suffix = pathinfo($fileName, PATHINFO_EXTENSION);

        if (!is_readable($fileName)) {
            throw new ConfigException(sprintf(self::$errors[10], $fileName), 10);
        }

        switch ($suffix) {
            case 'php':
                $data = include($fileName);
                break;
            case 'ini':
                $parser = new IniParser(file_get_contents($fileName));
                $data   = $parser->toArray();
                break;
            default:
                throw new ConfigException(sprintf(self::$errors[20], $suffix), 20);
        }

        if (!is_array($data)) {
            throw new ConfigException(sprintf(self::$errors[30], $fileName), 30);
        }

        return new self($data, $environment);;
    }
}
