<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.06.03.
 */

namespace Sky;

/**
 * Class Sky
 */
class Sky {
    const VENDOR  = 'Sky';
    const VERSION = '1.0';

    /**
     * Constructor
     */
    final private function __construct() {

    }
}
