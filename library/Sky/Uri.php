<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2015
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @link      https://bitbucket.org/spapp
 * @package   http_fw
 * @since     2015.06.03.
 */

namespace Sky;

/**
 * Class Uri
 *
 * @method string getScheme(string $aDefault = '')
 * @method $this setScheme(string $aScheme)
 * @method string getHost(string $aDefault = '')
 * @method $this setHost(string $aHost)
 * @method string getPort(int $aDefault = '')
 * @method string getUser(string $aDefault = '')
 * @method $this setUser(string $aUser)
 * @method string getPass(string $aDefault = '')
 * @method $this setPass(string $aPass)
 * @method string getPath(string $aDefault = '')
 * @method $this setPath(string $aPath)
 * @method string getQuery(string $aDefault = '')
 * @method string getFragment(string $aDefault = '')
 * @method $this setFragment(string $aFragment)
 */
class Uri {
    const PART_SCHEME   = 'scheme';
    const PART_HOST     = 'host';
    const PART_PORT     = 'port';
    const PART_USER     = 'user';
    const PART_PASS     = 'pass';
    const PART_PATH     = 'path';
    const PART_QUERY    = 'query';
    const PART_FRAGMENT = 'fragment';

    const GLUE_SCHEME    = '://';
    const GLUE_USER_INFO = '@';
    const GLUE_PORT      = ':';
    const GLUE_QUERY     = '?';
    const GLUE_FRAGMENT  = '#';
    /**
     * URI parts default values
     *
     * @var array
     * @static
     */
    private static $uriDefaults = array(
            self::PART_SCHEME   => '',
            self::PART_HOST     => '',
            self::PART_PORT     => '',
            self::PART_USER     => '',
            self::PART_PASS     => '',
            self::PART_PATH     => '/',
            self::PART_QUERY    => '',
            self::PART_FRAGMENT => ''
    );
    /**
     * @var array
     */
    private $uri;

    /**
     * Constructor
     *
     * @see Uri::setUri()
     *
     * @param string|array $aUri
     */
    public function __construct($aUri = '') {
        $this->setUri($aUri);
    }

    /**
     * Returns URI as a string
     *
     * @return string
     */
    public function toString() {
        $uri      = array();
        $scheme   = $this->getScheme();
        $query    = $this->getQuery();
        $fragment = $this->getFragment();

        if ($scheme) {
            array_push($uri, $scheme, self::GLUE_SCHEME);
        }

        array_push($uri, $this->getHierarchicalPart());

        if ($query) {
            array_push($uri, self::GLUE_QUERY, $query);
        }

        if ($fragment) {
            array_push($uri, self::GLUE_FRAGMENT, $fragment);
        }

        return implode('', $uri);
    }

    /**
     * Magice method
     *
     * @see Uri::toString()
     * @return string
     */
    final public function __toString() {
        return $this->toString();
    }

    /**
     * Magice method
     *
     * Allowed access to URI parts
     *
     * @param string $aName
     * @param array  $aArgs
     *
     * @return Uri|mixed
     * @throws \Exception
     */
    public function __call($aName, $aArgs) {
        $action = substr($aName, 0, 3);
        $part   = strtolower(substr($aName, 3));

        if (!array_key_exists($part, self::$uriDefaults)) {
            throw new \Exception('Not supported URI part');
        }

        switch ($action) {
            case 'get':
                return $this->get($part, (isset($aArgs[0]) ? $aArgs[0] : ''));
            case 'set':
                return $this->set($part, $aArgs[0]);
            default;
        }

        throw new \Exception('Not supported method');
    }

    /**
     * Returns the user info part of the URI
     *
     * @example username:password
     *
     * @return string
     */
    public function getUserInfo() {
        $username = $this->getUser();
        $password = $this->getPass();

        if ($username and $password) {
            return sprintf('%s:%s', $username, $password);
        }

        return '';
    }

    /**
     * Returns the authority part of the URI
     *
     * @example username:password@example.com:8042
     *
     * @return string
     */
    public function getAuthority() {
        $authority = array();
        $userInfo  = $this->getUserInfo();
        $port      = $this->getPort();

        if ($userInfo) {
            array_push($authority, $userInfo, self::GLUE_USER_INFO);
        }

        array_push($authority, $this->getHost());

        if ($port) {
            array_push($authority, self::GLUE_PORT, $port);
        }

        return implode('', $authority);
    }

    /**
     * Returns the hierarchical part of the URI
     *
     * @example authority part + path part
     *
     * @return string
     */
    public function getHierarchicalPart() {
        return $this->getAuthority() . $this->getPath();
    }

    /**
     * Returns the local part of the URI
     *
     * @return string
     */
    public function getLocalPart() {
        $part  = $this->getPath();
        $query = $this->getQuery();

        if ($query) {
            return $part . self::GLUE_QUERY . $query;
        }

        return $part;
    }

    /**
     * Sets port of URI
     *
     * @param int $aPort
     *
     * @return Uri
     */
    public function setPort($aPort) {
        return $this->set(self::PART_PORT, (int)$aPort);
    }

    /**
     * Sets query part of URI
     *
     * If gets an array, it will encode this array into a query string.
     *
     * @param string|array $aQuery
     *
     * @return Uri
     */
    public function setQuery($aQuery) {
        if (is_array($aQuery)) {
            $aQuery = http_build_query($aQuery);
        }

        return $this->set(self::PART_QUERY, $aQuery);
    }

    /**
     * Returns the query string as an associative array
     *
     * @return array
     */
    public function getQueryAsArray() {
        $query       = array();
        $queryString = $this->getQuery();

        if ($queryString) {
            parse_str($queryString, $query);
        }

        return $query;
    }

    /**
     * Sets and parse URI parts
     *
     * @param string|array $aUri
     *
     * @return $this
     */
    public function setUri($aUri) {
        if (!is_array($aUri)) {
            $aUri = parse_url($aUri);
        }

        $this->uri = array();

        foreach (self::$uriDefaults as $partName => $default) {
            if (isset($aUri[$partName]) and $aUri[$partName]) {
                $default = $aUri[$partName];
            }

            $fn = 'set' . ucfirst($partName);
            $this->{$fn}($default);
        }

        return $this;
    }

    /**
     * @param string $aUri
     *
     * @static
     * @return Uri
     */
    public static function create($aUri = '') {
        return new self($aUri);
    }

    /**
     * Returns specified part of URI
     *
     * If it is not exists then returns default value
     *
     * @param string $aName
     * @param string $aDefault
     *
     * @return mixed
     */
    final private function get($aName, $aDefault) {
        if (array_key_exists($aName, $this->uri)) {
            $aDefault = $this->uri[$aName];
        }

        return $aDefault;
    }

    /**
     * Sets specified part of URI
     *
     * @param string $aName
     * @param string $aValue
     *
     * @return $this
     */
    final private function set($aName, $aValue) {
        $this->uri[$aName] = $aValue;

        return $this;
    }
}
